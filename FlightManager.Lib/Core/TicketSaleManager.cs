﻿using FlightManager.Lib.Abstract;
using FlightManager.Lib.Model;
using System.Collections.Generic;

namespace FlightManager.Lib.Core
{
	public class TicketSaleManager : ITicketSaleManager
	{
		public void SellTicket(Flight flight, Passenger passenger)
		{
			if (!DataStorage.RegisteredPassengers.TryAdd(flight, new List<Passenger> {passenger}))
			{
				DataStorage.RegisteredPassengers[flight].Add(passenger);
			}
		}

		public void RefundTicket(Flight flight, Passenger passenger)
		{
			if (DataStorage.RegisteredPassengers.ContainsKey(flight))
			{
				DataStorage.RegisteredPassengers[flight].Remove(passenger);
			}
		}
	}
}
