﻿using FlightManager.Lib.Model;

namespace FlightManager.Lib.Abstract
{
	public interface ITicketSaleManager
	{
		public void SellTicket(Flight flight, Passenger passenger);
		public void RefundTicket(Flight flight, Passenger passenger);
	}
}
