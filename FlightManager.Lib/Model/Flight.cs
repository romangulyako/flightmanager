﻿using System;

namespace FlightManager.Lib.Model
{
	public class Flight
	{
		public Guid FlightNumber { get; set; } = Guid.NewGuid();
		public string AirLineName { get; set; }
		public string ArrivalPort { get; set; }
		public DateTime DepartureTime { get; set; }
		public int SeatsNumber { get; set; }

		public override string ToString()
		{
			return $"{FlightNumber} {AirLineName} {ArrivalPort}";
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as Flight);
		}

		protected bool Equals(Flight other)
		{
			return FlightNumber.Equals(other.FlightNumber)
					&& AirLineName == other.AirLineName
					&& ArrivalPort == other.ArrivalPort
					&& DepartureTime.Equals(other.DepartureTime)
					&& SeatsNumber == other.SeatsNumber;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(FlightNumber, AirLineName, ArrivalPort, DepartureTime, SeatsNumber);
		}
	}
}
